const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})


function volumetabung(r, t) {
  var jari = Number(r);
  var tinggi = Number(t);
  const hasilnya = 3.14 * (jari ** 2) * tinggi;
    console.log("Volume tabung adalah " + hasilnya + "\n");
  return hasilnya;
}

function volumekubus(s) {
  var sisi = Number(s);
  const hasil = sisi ** 3;
    console.log("Volume kubus adalah " + hasil);
  return hasil;
}

console.log("Menghitung Volume Tabung\n");
rl.question("Masukkan jari-jari tabung: ", r => {
  rl.question("Masukkan tinggi tabung: ", t => {
    volumetabung(r, t);
    console.log("Menghitung Volume Kubus\n");
    rl.question("Masukkan sisi kubus: ", s =>{
      volumekubus(s);

      rl.close()
    })
  })
})

rl.on ("close", () => {
  process.exit()
})
