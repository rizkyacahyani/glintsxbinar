let fridge = ["Tomato", "Broccoli", "Kale", "Cabbage", "Apple"]

for (var i = 0; i < fridge.length; i++) {
  if (fridge[i] !== "Apple") {
    console.log(`${fridge[i]} is a healthy food, it's definitely worth to eat`);
  }
}
