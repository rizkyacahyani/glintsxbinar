const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

function menu() {
  console.log("Menghitung Volume Bangun Ruang");

  console.log("Pilih Bangun Ruang\n");
  console.log("1. Tabung\n2. Kubus\n3. Exit");
  rl.question ("Masukkan pilihan anda: ", pilihan => {
    inputPilihan(pilihan);
  })
}

function inputPilihan(pilihan) {
  if (pilihan == 1) {
    inputJari();
  } else {
    if (pilihan == 2) {
      inputSisi()
    } else {
      console.log("Bye-bye!");
      rl.close()
    }
  }
}

function isKosongOrSpasi(str){
  return str == null || str.match(/^ *$/) !== null;
}

function hitunglagi(jawab) {
  if (jawab == "y") {
    menu();
  } else {
    if (jawab == "n") {
      console.log("Thank you!");
      rl.close()
    } else {
      console.log("Masukkan hanya y atau n!");
      rl.question("Ingin menghitung kembali? y/n ")
      hitunglagi()
    }

    }
  }


function volumetabung(r, t) {
  return 3.14 * (r ** 2) * t
}

function volumekubus(s) {
  return s ** 3
}

function inputJari() {
  rl.question("Masukkan jari-jari: ", r => {
    if (!isNaN(r) && !isKosongOrSpasi(r)) {
      inputTinggi(r)
    } else {
      console.log("Jari-jari harus berupa angka!\n");
      inputJari()
    }
  })
}

function inputTinggi(r) {
  rl.question("Masukkan tinggi: ", t => {
    if (!isNaN(t) && !isKosongOrSpasi(t)) {
      console.log("\nVolume tabung adalah: " + volumetabung(r, t));
      rl.question("Ingin menghitung kembali? y/n ", jawab => {
        hitunglagi(jawab);
      });
    } else {
      console.log("Tinggi harus berupa angka!\n");
      inputTinggi(r)
    }
  })
}

function inputSisi(s) {
  rl.question("Masukkan sisi: ", s => {
    if (!isNaN(s) && !isKosongOrSpasi(s)) {
      console.log("\nVolume kubus adalah: " + volumekubus(s));
      rl.question("Ingin menghitung kembali? y/n ", jawab => {
        hitunglagi(jawab);
      });
    } else {
      console.log("Sisi harus berupa angka!\n");
      inputSisi(s)
    }
  })
}

menu();
