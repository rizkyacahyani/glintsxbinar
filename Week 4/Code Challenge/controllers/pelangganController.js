const {
  Barang,
  Pelanggan,
  Transaksi
} = require("../models")
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

class PelangganController {

  constructor() {
    Pelanggan.hasMany(Transaksi, {
      foreignKey: 'id_pelanggan'
    })
    Barang.hasMany(Transaksi, {
      foreignKey: 'id_barang'
    })
    Transaksi.belongsTo(Pelanggan, {
      foreignKey: 'id_pelanggan'
    })
    Transaksi.belongsTo(Barang, {
      foreignKey: 'id_barang'
    })
  }

  // Get All data from transaksi
  async getAll(req, res) {
    Pelanggan.findAll({ // find all data of Transaksi table
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pelanggan,
        attributes: ['id', 'nama'] // just this attrubute from Pelanggan that showed
      }]
    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get all of Transaksi table
    })
  }

  // Get One data from transaksi
  async getOne(req, res) {
    Pelanggan.findOne({ // find one data of Transaksi table
      where: {
        id: req.params.id // where id of Transaksi table is equal to req.params.id
      },
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pelanggan,
        attributes: ['id', 'nama'] // just this attrubute from Pelanggan that showed
      }]
    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get one of Transaksi table depend on req.params.id
    })
  }

  // Create Transaksi data
  async create(req, res) {
    Pelanggan.findOne({ // find one data of Barang table
      where: {
        id: req.body.id_pelanggan // where id of Barang table is equal to req.params.id
      },
      attributes: ['nama'] // Get harga from Barang
    }).then(pelanggan => {
      res.json(pelanggan)
})
}

  // Update Transaksi data
  async update(req, res) {
    Pelanggan.findOne({ // find one data of Barang table
      where: {
        id: req.body.id_pelanggan // where id of Barang table is equal to req.params.id
      },
      attributes: ['nama'] // Get harga from Barang
    }).then(pelanggan => {
      res.json({
        "status": "success",
        "message": "pelanggan updated",
        "data": pelanggan
      })
    })
  }

  // Soft delete Transaksi data
  async delete(req, res) {
    Pelanggan.destroy({ // Delete data from Transaksi table
      where: {
        id: req.params.id // Where id of Transaksi table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "pelanggan deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }

}

module.exports = new PelangganController;
