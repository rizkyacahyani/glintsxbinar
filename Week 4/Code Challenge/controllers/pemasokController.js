const {
  Barang,
  Pemasok
} = require("../models")
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

class PemasokController {

  constructor() {
    Pemasok.hasMany(Barang, {
      foreignKey: 'id_pemasok'
    })
    Barang.belongsTo(Pemasok, {
      foreignKey: 'id_pemasok'
    })
  }

  // Get All data from transaksi
  async getAll(req, res) {
    Pemasok.findAll({ // find all data of Transaksi table
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pemasok,
        attributes: ['id'] // just this attrubute from Pelanggan that showed
      }]
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get all of Transaksi table
    })
  }

  // Get One data from transaksi
  async getOne(req, res) {
    Pemasok.findOne({ // find one data of Transaksi table
      where: {
        id: req.params.id // where id of Transaksi table is equal to req.params.id
      },
      attributes: ['id', 'nama', ['createdAt', 'waktu']], // just these attributes that showed
      include: [{
        model: Pemasok,
        attributes: ['id', 'nama'] // just this attrubute from Pelanggan that showed
      }]
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get one of Transaksi table depend on req.params.id
    })
  }

  // Create Transaksi data
  async create(req, res) {
    Pemasok.findOne({ // find one data of Barang table
      where: {
        id: req.body.id_pemasok // where id of Barang table is equal to req.params.id
      },
      attributes: ['nama'] // Get harga from Barang
    }).then(pemasok => {
      res.json(pemasok)
})
}

  // Update Transaksi data
  async update(req, res) {
    Pemasok.findOne({ // find one data of Barang table
      where: {
        id: req.body.id_pemasok // where id of Barang table is equal to req.params.id
      },
      attributes: ['nama'] // Get harga from Barang
    }).then(pemasok => {
      res.json({
        "status": "success",
        "message": "pemasok updated",
        "data": pemasok
      })
    })
  }

  // Soft delete Transaksi data
  async delete(req, res) {
    Pemasok.destroy({ // Delete data from Transaksi table
      where: {
        id: req.params.id // Where id of Transaksi table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "pemasok deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }

}

module.exports = new PemasokController;
