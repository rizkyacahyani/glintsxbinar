const connection = require('../models/connection.js')
// Make class TransaksiController
class BarangController {
  // Function getAll transaksi table
  async getAll(req, res) {
    try {
      var sql = " SELECT barang.id as id_barang, barang.nama as nama_barang, barang.harga, pemasok.nama as nama_pemasok FROM barang JOIN pemasok ON barang.id_pemasok = pemasok.id order by id_barang" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }



  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "SELECT barang.id as id_barang, barang.nama as nama_barang, barang.harga, pemasok.nama as nama_pemasok FROM barang JOIN pemasok ON barang.id_pemasok = pemasok.id WHERE barang.id = ?"
      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }
  async create(req, res) {
    try {
      connection.query(
        'INSERT INTO barang(nama,harga,id_pemasok) VALUES (?, ?, ?)', // ?terkhir  diambil dr boy total

        [req.body.nama_barang, req.body.harga, req.body.id_pemasok],
        (err, result) => { //ubh dsni
          if (err) throw err; // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )

    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function update transaksi table
  async update(req, res) {
    try {
      connection.query(
        'UPDATE barang SET nama = ?, harga = ?, id_pemasok = ? WHERE id = ?',
        [req.body.nama, req.body.harga, req.body.id_pemasok, req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )


    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }
  async delete(req, res) {
    try {
      var sql = 'DELETE FROM barang WHERE id = ?'

      connection.query(
        sql,
        [req.params.id],
        (err, result) => {
          if (err) {
            res.json({
              status: "Error",
              error: err
            });
          } // If error

          // If success it will return JSON of result
          res.json({
            status: 'Success',
            data: result
          })
        }
      )
    } catch (err) {
      // If error will be send Error JSON
      res.json({
        status: "Error",
        error: err
      })
    }
  }

}
module.exports = new BarangController;
