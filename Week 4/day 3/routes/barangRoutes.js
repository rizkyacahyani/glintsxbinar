const express = require('express')
const router = express.Router()
const BarangController = require('../controllers/barangController.js')

router.get('/', BarangController.getAll)
router.get('/:id', BarangController.getOne)
router.post('/create', BarangController.create)

module.exports = router
