const express = require('express')
const router = express.Router()
const PelangganController = require('../controllers/pelangganController.js')

router.get('/', PelangganController.getAll)
router.get('/:id', PelangganController.getOne)
router.post('/create', PelangganController.create)

module.exports = router
