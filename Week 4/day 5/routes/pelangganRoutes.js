const express = require('express') // Import express
const router = express.Router() // Make router from app
const PelangganController = require('../controllers/pelangganController.js') // Import TransaksiController
const pelangganValidator = require('../middlewares/validators/pelangganValidator.js') // Import validator to validate every request from user

router.get('/', PelangganController.getAll) // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get('/:id', PelangganController.getOne) // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post('/create', pelangganValidator.create, PelangganController.create) // If accessing localhost:3000/transaksi/create, it will call create function in TransaksiController class
router.put('/update/:id', pelangganValidator.update, PelangganController.update) // If accessing localhost:3000/transaksi/update/:id, it will call update function in TransaksiController class
router.delete('/delete/:id', PelangganController.delete) // If accessing localhost:3000/transaksi/delete/:id, it will call delete function in TransaksiController class

module.exports = router; // Export router
