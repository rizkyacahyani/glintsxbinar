const persegi = require ('./module/persegi.js')
const persegipanjang = require ('./module/persegipanjang.js')
const lingkaran = require ('./module/lingkaran.js')

const cobapersegi = new persegi(10)
console.log("Luas persegi adalah: " + cobapersegi.menghitungLuas() + "cm2");
console.log(`Keliling persegi adalah: ${cobapersegi.menghitungKeliling()} cm`);

const cobapersegipanjang = new persegipanjang(10, 20)
console.log(`Luas persegi panjang adalah: ${cobapersegipanjang.menghitungLuas()} cm2`);
console.log(`Keliling persegi panjang adalah: ${cobapersegipanjang.menghitungKeliling()} cm`);

const cobalingkaran = new lingkaran(8)
console.log(`Luas lingkaran adalah: ${cobalingkaran.menghitungLuas()} cm2` );
console.log(`Keliling lingkaran adalah ${cobalingkaran.menghitungKeliling()} cm`);
