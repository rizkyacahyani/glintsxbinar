const persegi = require ('./persegi.js')
const persegipanjang = require ('./persegipanjang.js')
const lingkaran = require ('./lingkaran.js')

class Module {

  constructor() {
    this.name = 'Menghitung Bangun Datar'
  }

  menghitungLuasPersegi(sisi) {
    let hitungLuasPersegi = new persegi(sisi)
    return hitungLuasPersegi.menghitungLuas()
  }

  menghitungKelilingPersegi(sisi) {
    let hitungKelilingPersegi = new persegi(sisi)
    return hitungKelilingPersegi.menghitungKeliling()
  }

  menghitungLuasPersegiPanjang(panjang, lebar) {
    let hitungLuasPersegiPanjang = new persegipanjang(panjang, lebar)
    return hitungLuasPersegiPanjang.menghitungLuas()
  }

  menghitungKelilingPersegiPanjang(panjang, lebar) {
    let hitungKelilingPersegiPanjang = new persegipanjang(panjang, lebar)
    return hitungKelilingPersegiPanjang.menghitungKeliling()
  }

  menghitungLuasLingkaran(radius) {
    let hitungLuasLingkaran = new lingkaran(radius)
    return hitungLuasLingkaran.menghitungLuas()
  }

  menghitungKelilingLingkaran(radius) {
    let hitungKelilingLingkaran = new lingkaran(radius)
    return hitungKelilingLingkaran.menghitungKeliling()
  }
}

module.exports = Module
