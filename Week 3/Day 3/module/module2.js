const tabung = require ('./tabung.js')
const kubus = require ('./kubus.js')
const balok = require ('./balok.js')

class Module {

  constructor() {
    this.name = 'Menghitung Bangun Ruang'
  }

  menghitungVolumeTabung(radius, tinggi) {
    let hitungVolumeTabung = new tabung(radius, tinggi)
    return hitungVolumeTabung.menghitungVolume()
  }

  menghitungVolumeKubus(sisi) {
    let hitungVolumeKubus = new kubus(sisi)
    return hitungVolumeKubus.menghitungVolume()
  }

  menghitungVolumeBalok(panjang, lebar, tinggi) {
    let hitungVolumeBalok = new balok(panjang, lebar, tinggi)
    return hitungVolumeBalok.menghitungVolume()
  }
}

module.exports = Module
