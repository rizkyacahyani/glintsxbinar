const bangundatar = require ('./bangundatar.js')

class PersegiPanjang extends bangundatar {

  constructor(panjang, lebar) {
    super('Persegi Panjang')
    this.panjang = panjang
    this.lebar = lebar
  }

  menghitungLuas() {
    super.menghitungLuas()
    return this.panjang * this.lebar
  }

  menghitungKeliling() {
    super.menghitungKeliling()
    return 2 * (this.panjang + this.lebar)
  }
}

module.exports = PersegiPanjang
