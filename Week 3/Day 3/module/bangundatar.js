const bangun = require ('./bangun.js')

class BangunDatar extends bangun {

  constructor(name) {
    super(name)

    if (this.constructor === BangunDatar) {
      throw new Error (`This is abstract!`)
    }

    this.name = name
  }

  menghitungLuas() {
    // console.log(`Luas bangun datar`);
  }

  menghitungKeliling() {
    // console.log(`Keliling bangun datar`);
  }
}

module.exports = BangunDatar
