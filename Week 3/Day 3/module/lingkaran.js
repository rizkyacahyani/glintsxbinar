const bangundatar = require ('./bangundatar.js')

class Lingkaran extends bangundatar {

  constructor(radius) {
    super('Lingkaran')
    this.radius = radius
  }

  menghitungLuas() {
    return Math.PI * Math.pow(this.radius, 2)
  }

  menghitungKeliling() {
    return 2 * Math.PI * this.radius
  }
}

module.exports = Lingkaran
