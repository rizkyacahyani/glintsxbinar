const bangun = require ('./bangun.js')

class BangunRuang extends bangun {

  constructor(name) {
    super(name)
    
    if (this.constructor === BangunRuang) {
      throw new Error (`This is abstract!`)
    }

    this.name = name
  }

  menghitungVolume() {
    // console.log(`Menghitung volume bangun ruang`);
  }
}

module.exports = BangunRuang
