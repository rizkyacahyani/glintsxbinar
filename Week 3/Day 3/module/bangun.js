class Bangun {

  constructor (name) {
    if (this.constructor === Bangun) {
      throw new Erro (`This is abstract!`)
    }
    this.name = name
  }

  menghitungVolume() {
    console.log(`Menghitung Volume`);
  }

  menghitungLuas() {
    console.log(`Menghitung Luas`);
  }

  menghitungKeliling() {
    console.log(`Menghitung Keliling`);
  }
}

module.exports = Bangun
