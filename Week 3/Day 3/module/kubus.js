const bangunruang = require ('./bangunruang.js')

class Kubus extends bangunruang {

  constructor(sisi) {
    super('Kubus')
    this.sisi = sisi
  }

  menghitungVolume() {
    return this.sisi ** 3
  }
}

module.exports = Kubus
