const bangundatar = require('./bangundatar.js')

class Persegi extends bangundatar {

  constructor(sisi) {
    super('Persegi')

    this.sisi = sisi
  }

  menghitungLuas() {
    super.menghitungLuas()
    return this.sisi ** 2
  }

  menghitungKeliling() {
    super.menghitungKeliling()
    return this.sisi *4
  }
}

module.exports = Persegi
