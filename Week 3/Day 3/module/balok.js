const bangunruang = require ('./bangunruang.js')

class Balok extends bangunruang {

  constructor(panjang, lebar, tinggi) {
    super('Balok')
    this.panjang = panjang
    this.lebar = lebar
    this.tinggi = tinggi
  }

  menghitungVolume() {
    return this.panjang * this.lebar * this.tinggi
  }
}

module.exports = Balok
