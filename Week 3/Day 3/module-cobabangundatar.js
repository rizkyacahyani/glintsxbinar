const importModule = require('./module/module.js')

const hitung = new importModule()

let luasPersegi = hitung.menghitungLuasPersegi(20)
console.log(`\nLuas Persegi adalah: ${luasPersegi} cm2`);

let kelilingPersegi = hitung.menghitungKelilingPersegi(10)
console.log(`Keliling Persegi adalah ${kelilingPersegi} cm`);

console.log(`\n=========\n`);

let luasPersegiPanjang = hitung.menghitungLuasPersegiPanjang(30, 18)
console.log(`Luas Persegi Panjang adalah: ${luasPersegiPanjang} cm2`);

let kelilingPersegiPanjang = hitung.menghitungKelilingPersegiPanjang(14, 23)
console.log(`Keliling Persegi Panjang adalah: ${kelilingPersegiPanjang} cm`);

console.log(`\n=========\n`);

let luasLingkaran = hitung.menghitungLuasLingkaran(19)
console.log(`Luas Lingkaran adalah: ${luasLingkaran} cm2`);

let kelilingLingkaran = hitung.menghitungKelilingLingkaran(18)
console.log(`Keliling Lingkaran adalah: ${kelilingLingkaran} cm\n`);
