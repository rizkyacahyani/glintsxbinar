const importModule = require ('./module/module2.js')

const hitung = new importModule()

let volumeTabung = hitung.menghitungVolumeTabung(12, 34)
console.log(`\nVolume tabung adalah: ${volumeTabung} cm3`);

console.log(`\n==========\n`);

let volumeKubus = hitung.menghitungVolumeKubus(9)
console.log(`Volume kubus adalah: ${volumeKubus} cm3`);

console.log(`\n==========\n`);

let volumeBalok = hitung.menghitungVolumeBalok(12, 5, 32)
console.log(`Volume balok adalah: ${volumeBalok} cm3\n`);
