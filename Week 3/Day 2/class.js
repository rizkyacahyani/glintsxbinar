class MobilePhone {

  static screen = true // Static variable

// Constructor
  constructor(name, brand, price, specs) {
    this.name = name
    this.brand = brand
    this.price = price
    this.specs = specs
  }

  // Static Method/Function
  static introduce() {
  console.log(`This is phone`);
  }

  // Instance Method
  hello() {
    console.log(`I am ${this.name} from ${this.brand}`);
  }

  myspecs(){
    console.log(`This is my specs, ${this.specs.ram} RAM and ${this.specs.processor} processor`);
  }

  myprice(){
    console.log(`My price is IDR ${this.price}`);
  }

  ad() {
    // this.introduce() - will error because it's Static
    this.hello()
    this.myspecs()
    this.myprice()
  }
  // end of instance method
}

// Add instance method out of class
MobilePhone.prototype.banting = function() {
  console.log(`Pecah dong`);
}

// Add static method out of class
MobilePhone.fall = function() {
  console.log(`Rusak dong`);
}

module.exports = MobilePhone
