class BangunDatar {

  constructor(name) {
    this.name = name
  }

  luas() {
    console.log(`Luas bangun datar`);
  }

  keliling() {
    console.log(`Keliling bangun datar`);
  }
}

// This class is child of BangunDatar
class PersegiPanjang extends BangunDatar {

  constructor(panjang, lebar) {
    super(`Persegi Panjang`)
    this.panjang = panjang
    this.lebar = lebar
  }

  luas() {
    return this.panjang * this.lebar
  }

  keliling() {
    return 2 * (this.panjang + this.lebar)
  }
}

let persegipanjang = new PersegiPanjang (10, 10)
console.log(persegipanjang.luas());
console.log(persegipanjang.keliling());
