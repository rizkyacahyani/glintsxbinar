const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})

const
  read = readFile('utf-8')
files = ['content/content1.txt', 'content/content.txt', 'content/content3.txt', 'content/content5.txt']

Promise.allSettled(files.map(file => read(`${file}`)))
  .then(result => {
    console.log(result);
  })
