const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
  fs.readFile(file, options, (err, content) => {
    if (err) return reject(err)
    return resolve(content)
  })
})

const writeFile = (file, content) => new Promise((resolve, reject) => {
  fs.writeFile(file, content, err => {
    if (err) return reject(err)
    return resolve()
  })
})

const read = readFile('utf-8')

Promise.race([read('content/content1.txt'), read('content/content2.txt'), read('content/content3.txt'), read('content/content4.txt'),
    read('content/content5.txt'), read('content/content6.txt'), read('content/content7.txt'), read('content/content8.txt'),
    read('content/content9.txt'), read('content/content10.txt')
  ])
  .then((value) => {
    console.log(value);
  })
  .catch(error => {
    console.log(error);
  })
