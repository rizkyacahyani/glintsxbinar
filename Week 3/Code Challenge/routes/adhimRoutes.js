const express = require ('express')
const router = express.Router()
const AdhimController = require ('../controllers/adhimController.js')

router.get('/', AdhimController.hello)

module.exports = router
