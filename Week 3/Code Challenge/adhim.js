const express = require ('express')
const app = express()
const adhimRoutes = require ('./routes/adhimRoutes.js')

app.use('/', adhimRoutes)

app.listen(8080)
