const express = require ('express')
const router = express.Router()
const IndexController = require('../controllers/indexController.js')

router.get('/index', IndexController.index)
router.get('/', IndexController.indexHome)

module.exports = router;
