class IndexController {

  async index(req, res) {
    try {
      console.log('You are accessing index');
      res.render('index.ejs')
    } catch (e) {
      res.status(500).send(exception)
    }
  }

  async indexHome(req, res) {
    try {
      console.log('You are accessing Hello World!');
      res.render('top.ejs')
    } catch (e) {
      res.status(500).send(exception)
    }
  }
}

module.exports = new IndexController
