// Import express
const express = require ('express')
const app = express()
const helloRoutes = require ('./routes/helloRoutes.js')
const indexRoutes = require ('./routes/indexRoutes.js')

// use public directory for images and other files
app.use(express.static('public'))


// if the user request to url localhost:3000
app.use('/', indexRoutes)

// if the user request to localhost:3000/heloo
app.use('/hello', helloRoutes)

// if user request to url localhost:3000/index
app.use('/index', indexRoutes)

app.listen(3000) // 3000 adalah port number
