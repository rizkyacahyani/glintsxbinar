const EventEmitter = require('events')
const readline = require('readline')

const my = new EventEmitter()
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const covid = require('./suspect.js')

const loginFailed = (email) => {
  console.log(`${email} tidak bisa masuk`);
  rl.close()
}

my.on(`Login Failed`, loginFailed)

const user = {
  login(email, password) {
    const pass = '123456'

    if (password != pass) {
      my.emit(`Login Failed`, email)
    } else {
      console.log(`Berhasil login`);
      covid.menu()
    }
  }
}

rl.question(`Email: `, email => {
  rl.question(`Password: `, password => {
    user.login(email, password)
  })
})

module.exports.rl = rl
