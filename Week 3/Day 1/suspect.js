// const readline = require('readline');
// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// })
const rl = require('./coba-event.js')

let array = [{
    name: "1. Jejen",
    status: "Positive"
  }, {
    name: "1. Jojon",
    status: "Negative"
  },
  {
    name: "1. Jujun",
    status: "Suspect"
  }, {
    name: "2. Pikitong",
    status: "Positive"
  },
  {
    name: "3. Prikitiw",
    status: "Positive"
  }, {
    name: "2. Dipsy",
    status: "Negative"
  },
  {
    name: "3. Tinky Winky",
    status: "Negative"
  }, {
    name: "2. Si Agus",
    status: "Suspect"
  },
  {
    name: "3. Pikiti",
    status: "Suspect"
  }
]

function positive() {
  console.log(`\nDaftar orang yang positive adalah:`);
  for (var i = 0; i < array.length; i++) {
    if (array[i].status == "Positive") {
      console.log(`${array[i].name}`);
    }
  }
}

function negative() {
  console.log(`\nDaftar orang yang negative adalah:`);
  for (var i = 0; i < array.length; i++) {
    if (array[i].status == "Negative") {
      console.log(`${array[i].name}`);
    }
  }
}

function suspect() {
  console.log(`\nDaftar orang yang suspect adalah:`);
  for (var i = 0; i < array.length; i++) {
    if (array[i].status == "Suspect") {
      console.log(`${array[i].name}`);
    }
  }
}

function masuklagi() {
  rl.rl.question("Mau lagi? y/n ", jawab => {
    if (jawab == "y") {
      inimenunya();
    } else {
      if (jawab == "n") {
        console.log("Terima kasih. Semoga anda sehat selalu.");
        rl.rl.close()
      }
    }
  })
}

function inimenunya() {
  console.log(`Selamat Datang di Database Pasien Rumah Sakit "Tapi Sehat"`);
  console.log(`==========================================================\n`);
  console.log(`Pilih Daftar Pasien Menurut Status Diagnosis`);
  console.log(`1. Positive\n2. Negative\n3. Suspect\n4. Exit`);
  rl.rl.question(`Masukkan pilihan anda: `, pilihan => {
    switch (Number(pilihan)) {
      case 1:
        positive();
        masuklagi()
        break;
      case 2:
        negative()
        masuklagi()
        break;
      case 3:
        suspect()
        masuklagi()
        break;
      case 4:
        console.log(`Terima kasih. Semoga anda sehat selalu.`);
        rl.rl.close()
        break;
      default:
        inimenunya()
    }

  })

}

// function pilihlagi() {
//   rl.question("Apakah anda ingin melihat daftar lainnya? [y/n] : ", pilihannya => {
//     if (pilihannya == `y`) {
//       return inimenunya();
//     } else if (pilihannya == `n`) {
//       console.log("Terima kasih. Semoga anda sehat selalu.");
//     } else {
//       console.log("Masukkan hanya y atau n");
//       pilihlagi()
//     }
//   })
// }

// inimenunya();

module.exports.menu = inimenunya
