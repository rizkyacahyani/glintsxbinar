const {
  barang,
  pemasok
} = require('../models')

class BarangController {

  async getAll(req, res) {
    barang.find({}).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async getOne(req, res) {
    barang.findOne({
      _id: req.params.id
    }).then(result => {
      res.json({
        status: "Success",
        data: result
      })
    })
  }

  async create(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      }),
    ])

    barang.create({
      nama: req.body.nama,
      harga: req.body.harga,
      pemasok: data
    }).then(result => {
      res.json({
        status: "Success add data",
        data: result
      })
    })
  }

  async update(req, res) {
    const data = await Promise.all([
      pemasok.findOne({
        _id: req.body.id_pemasok
      })
    ])

    barang.findOneAndUpdate({
      _id: req.params.id
    }, {
      nama: req.body.nama,
      harga: req.body.harga,
      pemasok: data
    }).then(() => {
      return barang.findOne({
        _id: req.params.id
      })
    }).then(result => {
      res.json({
        status: "Success update data",
        data: result
      })
    })
  }

  async delete(req, res) {
    barang.delete({
      _id: req.params.id
    }).then(() => {
      res.json({
        status: "Success delete data",
        data: null
      })
    })
  }
}

module.exports = new BarangController
