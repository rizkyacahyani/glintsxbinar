const express = require('express')
const router = express.Router()
const BarangController = require('../controllers/barangController')
const barangValidator = require('../middlewares/validators/barangValidator')

router.get('/', BarangController.getAll)
router.get('/:id', barangValidator.getOne, BarangController.getOne)
router.post('/create', barangValidator.create, BarangController.create)
router.put('/update/:id', barangValidator.update, BarangController.update)
router.delete('/delete/:id', barangValidator.delete, BarangController.delete)

module.exports = router;
