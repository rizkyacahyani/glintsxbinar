const {
  barang,
  pemasok
} = require('../models')
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator')

module.exports = {
    getOne: [
      check('id').custom(value => {
        return barang.findOne({
          _id: value
        }).then(result => {
          if (result.length == 0) {
            throw new Error('ID barang tidak ada!')
          }
        })
      }),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          });
        }
        next();
      },
    ],

    create: [
      check('id_pemasok').custom(value => {
        return pemasok.findOne({
          _id: value
        }).then(p => {
          if (!p) {
            throw new Error('ID pemasok tidak ada!')
          }
        })
      }),
      check('nama').isString(),
      (req, res, next) => {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          })
        }
      },
      check('harga').isNumeric(),
      (req, res, next) => {
        const errors = validationResult(req);
        if(!errors.isEmpty()) {
          return res.status(422).json({
            errors: errors.mapped()
          })
        }
        next();
      }
    ]
