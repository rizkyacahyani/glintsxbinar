const { MongoClient } = require("mongodb");

// Connection URI
const uri =
  "mongodb://localhost:27017/";

// Create a new MongoClient
const client = new MongoClient(uri,{useUnifiedTopology:true});

client.connect();


module.exports = client;
