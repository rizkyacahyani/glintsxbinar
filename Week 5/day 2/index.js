const express = require ('express')
const app = express()
const bodyParser = require('body-parser');
const transaksiRoutes = require ('./routes/transaksiRoutes.js')
const pemasokRoutes = require ('./routes/pemasokRoutes.js')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}))

app.use(express.static('public'))

app.use('/transaksi', transaksiRoutes)
app.use('/pemasok', pemasokRoutes)

app.listen(3000)
